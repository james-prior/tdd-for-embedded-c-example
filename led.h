#undef Global
#undef Init
#if defined(LED_C)
#define Global
#define Init(x) =(x)
#else
#define Global extern
#define Init(x)
#endif

Global void initialize_led(
    volatile uint8_t *led_output_direction,
    volatile uint8_t *led);
Global void turn_on_led(volatile uint8_t *led);
Global void turn_off_led(volatile uint8_t *led);
