/* register definitions for Arduino UNO r3 */

#include "atmega328.h"

#define LED_BIT_INDEX (5)
#define led_output_direction_port (portb_output_direction)
#define LED_OUTPUT_DIRECTION_MASK (1 << LED_BIT_INDEX)
#define led_output_port (portb_output)
#define LED_ON_MASK (1 << LED_BIT_INDEX)
