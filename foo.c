#define FOO_C
#include <stdlib.h>
#include <stdint.h>
#include "io.h"
#include "led.h"
#include "foo.h"

void initialize_everything(void)
{
    initialize_led(io.led_output_direction_port_p, io.led_output_port_p);
}

void loop_body(void)
{
    turn_on_led(io.led_output_port_p);
    turn_off_led(io.led_output_port_p);
}

void infinite_loop(void) /* can not test this */
{
    for (;;)
        loop_body();
}
