#undef Global
#undef Init
#if defined(IO_C)
#define Global
#define Init(x) =(x)
#else
#define Global extern
#define Init(x)
#endif

#include "arduino-uno-r3.h"

struct io_struct {
    volatile uint8_t *led_output_direction_port_p;
    volatile uint8_t *led_output_port_p;
};

extern struct io_struct io;
