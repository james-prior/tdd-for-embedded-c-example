# Test-Driven Development for Embedded C

This provides an example of what test-driven code looks like for
embedded C on a microcontroller.

The oversimplification of TDD for embedded C is to test:
- what you can
- when you can
- where you can

Key to that is a hybrid approach of running unit tests frequently
on a development computer and running the code on the target
embedded hardware.

three techniques
- linker trickery
- function pointer trickery
- #define trickery

portable exact precision data types For example:
- uint8_t
- uint16_t
- uint32_t

arduino-cli versus this

start with turn_on_led arduino-cli versus this

can not test:
- main()
  - because test runner program needs to use that name
- infinite loop
  - because test would never finish
    - avoided fudges such as while (flag)

## Uses command line interface (CLI) tools.

This example uses Command Line Interface (CLI) tools to develop
software for the target Arduino UNO r3 boards without using the
Arduino environment of tools, files and libraries.

I use only open source tools.

Uses [Unity](http://www.throwtheswitch.org/unity) framework.

---

This example is based on an
[Arduino UNO](https://www.arduino.cc/en/Guide/ArduinoUno)
because the Arduino UNO r3s are
-   [open source hardware](https://en.wikipedia.org/wiki/Open-source_hardware),
-   plentiful,
-   cheap,
-   and not too complex.
and because there are excellent open source tools for UNO r3s.

I have both a genuine Arduino UNO r3 and some
[legitimate clones from Microcenter](https://www.microcenter.com/product/486544/inland-uno-r3-mainboard).
The Microcenter clones are so good that the Arduino tools can not distinguish
them from the genuine Arduino boards.

Microcenter clones:

good:
-   [cheaper (<$10)](https://www.microcenter.com/product/486544/inland-uno-r3-mainboard) than [genuine Arduino UNO r3 for $23](https://store.arduino.cc/usa/arduino-uno-rev3)
-   behave _exactly_ the same as genuine Arduino UNO r3
-   have a DIP chip which is easy to clip to pins on
-   the microcontroller is socketed which makes them easy to
    replace or remove for use in other boards.
-   in stock at local retail store (Columbus and Madison Heights)

bad:
-   not as [cheap](https://www.alibaba.com/product-detail/Development-Starter-Kit-ATmega16U2-Board-Atmega328P_62234441536.html) as [buying directly from China](https://www.alibaba.com/trade/search?SearchText=uno+r3), especially in quantity
