#undef Global
#undef Init
#if defined(LOGGER_C)
#define Global
#define Init(x) =(x)
#else
#define Global extern
#define Init(x)
#endif

Global char *open_log(int n);
Global char *get_log(void);
Global void log_string(const char *s);
