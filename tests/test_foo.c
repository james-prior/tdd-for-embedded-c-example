#include <stdint.h>
#include "unity.h"
#include "io.h"
#include "foo.h"
#include "tests/logger.h"

void setUp(void)
{
    open_log(1000);
    io.led_output_direction_port_p = (volatile uint8_t *)0xDEADBEEF;
    io.led_output_port_p = (volatile uint8_t *)0x01234567;
}

void tearDown(void)
{
}

void test_initialize_everything_calls_initialize_led_with_args(void)
{
    const char *expected_log = "initialize_led(0xDEADBEEF, 0x01234567)\n";
    char *actual_log;

    initialize_everything();

    actual_log = get_log();
    TEST_ASSERT_EQUAL_STRING(expected_log, actual_log);
}

void test_loop_body_turn_on_led_then_turn_off_led(void)
{
    const char *expected_log = (
        "turn_on_led(0x01234567)\n"
        "turn_off_led(0x01234567)\n"
    );
    char *actual_log;

    loop_body();

    actual_log = get_log();
    TEST_ASSERT_EQUAL_STRING(expected_log, actual_log);
}
