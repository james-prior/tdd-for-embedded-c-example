#define TESTABLE_MALLOC_C
#include <stdlib.h>
/*
#include <stdint.h>
#include "io.h"
#include "foo.h"
#include "testable_main.h"
*/

void *testable_malloc(size_t size)
{
    return malloc(size);
}

void testable_free(void *ptr)
{
    free(ptr);
}
