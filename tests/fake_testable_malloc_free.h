#undef Global
#undef Init
#if defined(FAKE_MALLOC_FREE_C)
#define Global
#define Init(x) =(x)
#else
#define Global extern
#define Init(x)
#endif

Global void clear_testable_malloc_free_log(void);
Global char *get_testable_malloc_free_log(void);
Global void *testable_malloc(size_t size);
Global void testable_free(void *ptr);
