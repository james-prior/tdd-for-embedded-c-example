#undef LOGGER_C
#include <stdlib.h>
/*
#include <stdint.h>
*/
#include "unity.h"
#include "tests/fake_testable_malloc_free.h"
#include "tests/logger.h"

/*
todo list
if open_log is called and log is not NULL, call free(log)
*/

void test_first_open_log_just_calls_malloc(void)
{
    const char *expected_log = (
        "testable_free(0x0)\n"
        "testable_malloc(0x6)\n"
    );
    char *actual_log;
    char *p;

    clear_testable_malloc_free_log();

    p = open_log(6);

    actual_log = get_testable_malloc_free_log();
    TEST_ASSERT_EQUAL_STRING(expected_log, actual_log);
    /* free(p); must leak from this test to next test */
}

void test_second_open_log_just_calls_free_then_malloc(void)
{
    char *actual_log;
    char *expected_log;
    char *p;

    clear_testable_malloc_free_log();
    p = open_log(7);
    asprintf(
        &expected_log,
        (
            "testable_free(0x%lX)\n"
            "testable_malloc(0x8)\n"
        ),
        (unsigned long)p
    );
    clear_testable_malloc_free_log();
    open_log(8);

    actual_log = get_testable_malloc_free_log();
    TEST_ASSERT_EQUAL_STRING(expected_log, actual_log);
    /*
    */
}
