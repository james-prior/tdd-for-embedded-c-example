#undef TESTABLE_MAIN_C
#include <stdlib.h>
#include <stdint.h>
#include "unity.h"
#include "io.h"
#include "testable_main.h"
#include "tests/logger.h"

void setUp(void)
{
    open_log(1000);
}

void tearDown(void)
{
}

void test_testable_main_calls_initialize_everything_then_infinite_loop_then_return_exit_failure(void)
{
    const char *expected_log = (
        "initialize_everything()\n"
        "infinite_loop()\n"
    );
    char *actual_log;
    int expected_return_value = EXIT_FAILURE;
    int actual_return_value;

    actual_return_value = testable_main();

    actual_log = get_log();
    TEST_ASSERT_EQUAL_INT(expected_return_value, actual_return_value);
    TEST_ASSERT_EQUAL_STRING(expected_log, actual_log);
}
