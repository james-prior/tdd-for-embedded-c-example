#undef LOGGER_C
#include <stdlib.h>
#include <stdint.h>
#include "unity.h"
#include "tests/logger.h"

/*
todo list
if open_log is called and log is not NULL, call free(log)
*/

void test_get_log_returns_null(void)
{
    char *actual_log;

    actual_log = get_log();

    TEST_ASSERT_NULL(actual_log);
}

void test_open_log_returns_non_null(void)
{
    char *actual_log;

    actual_log = open_log(8);

    TEST_ASSERT_NOT_NULL(actual_log);
}

void test_open_log_then_get_log_return_same_non_null(void)
{
    char *actual_log_1;
    char *actual_log_2;

    actual_log_1 = open_log(8);
    actual_log_2 = get_log();

    TEST_ASSERT_EQUAL_INT(actual_log_1, actual_log_2);
}

void test_open_log_then_log_string_then_get_log_returns_same_string(void)
{
    const char *expected = "Hello!\n";
    char *actual_log;

    open_log(8);
    log_string("Hello!\n");
    actual_log = get_log();

    TEST_ASSERT_EQUAL_STRING(expected, actual_log);
}

void test_open_log_then_twice_log_string_then_get_log_returns_cat_string(void)
{
    const char *expected = "Hello world!\n";
    char *actual_log;

    open_log(20);
    log_string("Hello");
    log_string(" world!\n");
    actual_log = get_log();

    TEST_ASSERT_EQUAL_STRING(expected, actual_log);
}

void test_open_log_then_thrice_log_string_then_get_log_returns_cat_string(void)
{
    const char *expected = "foo\nbar\nbaz\n";
    char *actual_log;

    open_log(20);
    log_string("foo\n");
    log_string("bar\n");
    log_string("baz\n");
    actual_log = get_log();

    TEST_ASSERT_EQUAL_STRING(expected, actual_log);
}

void test_open_log_0_returns_null(void)
{
    char *actual_log;

    actual_log = open_log(0);

    TEST_ASSERT_NULL(actual_log);
}

void test_open_log_then_log_string_of_long_string_logs_truncated_string(void)
{
    const char *expected_log = "Hello";
    char *actual_log;

    actual_log = open_log(6);
    log_string("Hello world!\n");

    TEST_ASSERT_EQUAL_STRING(expected_log, actual_log);
}

void test_open_log_then_twice_log_string_of_string_logs_truncated_string(void)
{
    const char *expected_log = "Hello wor";
    char *actual_log;

    actual_log = open_log(10);
    log_string("Hello");
    log_string(" world!\n");

    TEST_ASSERT_EQUAL_STRING(expected_log, actual_log);
}
