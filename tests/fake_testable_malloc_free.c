#define FAKE_MALLOC_FREE_C
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "fake_testable_malloc_free.h"

static char testable_malloc_free_log[100];

void clear_testable_malloc_free_log(void)
{
    testable_malloc_free_log[0] = '\0';
}

char *get_testable_malloc_free_log(void)
{
    return testable_malloc_free_log;
}

void *testable_malloc(size_t size)
{
    char *s;

    asprintf(
        &s,
        "%s(0x%lX)\n",
        __FUNCTION__,
        (unsigned long)size
    );
    strcat(testable_malloc_free_log, s);
    free(s);

    return malloc(size);
}

void testable_free(void *ptr)
{
    char *s;

    asprintf(
        &s,
        "%s(0x%lX)\n",
        __FUNCTION__,
        (unsigned long)ptr
    );
    strcat(testable_malloc_free_log, s);
    free(s);

    free(ptr);
}
