#undef TESTABLE_MAIN_C
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include "logger.h"
#include "testable_main.h"

void initialize_everything(void)
{
    char *s;

    asprintf(
        &s,
        "%s()\n",
        __FUNCTION__
    );
    log_string(s);
    free(s);
}

void infinite_loop(void)
{
    char *s;

    asprintf(
        &s,
        "%s()\n",
        __FUNCTION__
    );
    log_string(s);
    free(s);
}
