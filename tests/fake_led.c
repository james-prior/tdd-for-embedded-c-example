#define LED_C
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include "logger.h"
#include "led.h"

void initialize_led(
    volatile uint8_t *led_output_direction,
    volatile uint8_t *led)
{
    char *s;

    asprintf(
        &s,
        "%s(0x%08lX, 0x%08lX)\n",
        __FUNCTION__,
        (unsigned long)led_output_direction,
        (unsigned long)led
    );
    log_string(s);
    free(s);
}

void turn_on_led(volatile uint8_t *led)
{
    char *s;

    asprintf(
        &s,
        "%s(0x%08lX)\n",
        __FUNCTION__,
        (unsigned long)led
    );
    log_string(s);
    free(s);
}

void turn_off_led(volatile uint8_t *led)
{
    char *s;

    asprintf(
        &s,
        "%s(0x%08lX)\n",
        __FUNCTION__,
        (unsigned long)led
    );
    log_string(s);
    free(s);
}
