#define IO_C
#include <stdint.h>
#include "io.h"

volatile uint8_t fake_led_output_direction_port;
volatile uint8_t fake_led_output_port;

Global struct io_struct io = {
    &fake_led_output_direction_port,
    &fake_led_output_port
};
