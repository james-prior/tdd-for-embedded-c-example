#undef Global
#undef Init
#if defined(TESTABLE_MALLOC_C)
#define Global
#define Init(x) =(x)
#else
#define Global extern
#define Init(x)
#endif

Global void *testable_malloc(size_t size);
Global void testable_free(void *ptr);
