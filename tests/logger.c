#define LOGGER_C
#include <stdlib.h>
#include <string.h>
#include "tests/testable_malloc_free.h"
#include "tests/logger.h"

#define FALSE (0)
#define TRUE (!FALSE)

#define MINUMUM_LOG_LENGTH (1)

static char *log = NULL;
static int log_length = 0;

char *open_log(int n)
{
    testable_free(log);
    if (n < MINUMUM_LOG_LENGTH) {
        log = NULL;
        log_length = 0;
        return log;
    }
    log_length = n;
    log = testable_malloc(log_length);
    log[0] = '\0';
    return log;
}

char *get_log(void)
{
    return log;
}

void log_string(const char *s)
{
    int n;

    n = (log_length - 1) - strlen(log);
    if (n < 0)
        return;

    strncat(log, s, n);
}
