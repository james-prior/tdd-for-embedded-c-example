#include <stdint.h>
#include "led.h"
#include "unity.h"

void setUp(void)
{
}

void tearDown(void)
{
}

void test_turnOnLed_setsCorrectBitWhenStartingAllBitsLow(void)
{
    uint8_t fake_led = 0;

    turn_on_led(&fake_led);
    TEST_ASSERT_EQUAL_HEX(0x20, fake_led);
}

void test_turnOnLed_keepsCorrectBitHighWhenAllBitsHigh(void)
{
    uint8_t fake_led = ~0;

    turn_on_led(&fake_led);
    TEST_ASSERT_EQUAL_HEX(0xFF, fake_led);
}

void test_turnOnLed_setsOnlyItsBitPreservingOtherBits(void)
{
    uint8_t fake_led = 0x55;

    turn_on_led(&fake_led);
    TEST_ASSERT_EQUAL_HEX(0x75, fake_led);
}

void test_turnOffLed_keepsCorrectBitLowWhenStartingAllBitsLow(void)
{
    uint8_t fake_led = 0;

    turn_off_led(&fake_led);
    TEST_ASSERT_EQUAL_HEX(0x00, fake_led);
}

void test_turnOffLed_clearsCorrectBitLowWhenStartingAllBitsHigh(void)
{
    uint8_t fake_led = ~0;

    turn_off_led(&fake_led);
    TEST_ASSERT_EQUAL_HEX(0xDF, fake_led);
}

void test_turnOffLed_clearsOnlyItsBitPreservingOtherBits(void)
{
    uint8_t fake_led = 0xAA;

    turn_off_led(&fake_led);
    TEST_ASSERT_EQUAL_HEX(0x8A, fake_led);
}
