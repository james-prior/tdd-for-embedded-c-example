#define TESTABLE_MAIN_C
#include <stdlib.h>
#include <stdint.h>
#include "io.h"
#include "foo.h"
#include "testable_main.h"

int testable_main(void)
{
    initialize_everything();

    infinite_loop();

    return EXIT_FAILURE;
}
