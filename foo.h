#undef Global
#undef Init
#if defined(FOO_C)
#define Global
#define Init(x) =(x)
#else
#define Global extern
#define Init(x)
#endif

Global void initialize_everything(void);
Global void loop_body(void);
Global void infinite_loop(void);
