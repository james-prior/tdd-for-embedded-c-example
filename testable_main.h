#undef Global
#undef Init
#if defined(TESTABLE_MAIN_C)
#define Global
#define Init(x) =(x)
#else
#define Global extern
#define Init(x)
#endif

Global int testable_main(void);
