#define IO_C
#include <stdint.h>
#include "io.h"

Global struct io_struct io = {
    &led_output_direction_port,
    &led_output_port
};
