# ==========================================
#   Unity Project - A Test Framework for C
#   Copyright (c) 2007 Mike Karlesky, Mark VanderVoord, Greg Williams
#   [Released under MIT License. Please refer to license.txt for details]
# ==========================================

#We try to detect the OS we are running on, and adjust commands as needed
ifeq ($(OS),Windows_NT)
  ifeq ($(shell uname -s),) # not in a bash-like shell
	CLEANUP = del /F /Q
	MKDIR = mkdir
  else # in a bash-like shell, like msys
	CLEANUP = rm -f
	MKDIR = mkdir -p
  endif
	TEST_TARGET_EXTENSION=.exe
else
	CLEANUP = rm -f
	MKDIR = mkdir -p
	TEST_TARGET_EXTENSION=.out
endif

# The following line avoids deleting intermediate files when done.
.SECONDARY:

.PHONY: burn clean clean-target tests clean-tests run-tests

###############################################################################
# test stuff

TEST_C_COMPILER=gcc
ifeq ($(shell uname -s), Darwin)
TEST_C_COMPILER=clang
endif

UNITY_ROOT=unity

TEST_CFLAGS=-std=c89
TEST_CFLAGS += -Wall
TEST_CFLAGS += -Wextra
TEST_CFLAGS += -Wpointer-arith
TEST_CFLAGS += -Wcast-align
TEST_CFLAGS += -Wwrite-strings
TEST_CFLAGS += -Wswitch-default
TEST_CFLAGS += -Wunreachable-code
TEST_CFLAGS += -Winit-self
TEST_CFLAGS += -Wmissing-field-initializers
TEST_CFLAGS += -Wno-unknown-pragmas
TEST_CFLAGS += -Wstrict-prototypes
TEST_CFLAGS += -Wundef
TEST_CFLAGS += -Wold-style-definition
#TEST_CFLAGS += -Wno-misleading-indentation

TEST_TARGET1 = test_testable_main$(TEST_TARGET_EXTENSION)
TEST_TARGET2 = test_foo$(TEST_TARGET_EXTENSION)
TEST_TARGET3 = test_led$(TEST_TARGET_EXTENSION)
TEST_TARGET4 = test_logger$(TEST_TARGET_EXTENSION)
TEST_TARGET5 = test_logger_leak$(TEST_TARGET_EXTENSION)
TEST_INC_DIRS=-I. -I$(UNITY_ROOT)/src
TEST_SYMBOLS=

tests: run-tests

run-tests:  $(TEST_TARGET1) $(TEST_TARGET2) $(TEST_TARGET3) $(TEST_TARGET4) $(TEST_TARGET5)
	- ./$(TEST_TARGET1)
	- ./$(TEST_TARGET2)
	- ./$(TEST_TARGET3)
	- ./$(TEST_TARGET4)
	- ./$(TEST_TARGET5)

$(TEST_TARGET1): $(UNITY_ROOT)/src/unity.c tests/test_testable_main.c tests/fake_io.c tests/fake_foo.c tests/logger.c tests/testable_malloc_free.c tests/runners/test_testable_main_1_runner.c test-production.a
	$(TEST_C_COMPILER) $(TEST_CFLAGS) $(TEST_INC_DIRS) $(TEST_SYMBOLS) $^ -o $@

$(TEST_TARGET2): $(UNITY_ROOT)/src/unity.c tests/test_foo.c tests/fake_io.c tests/fake_led.c tests/logger.c tests/testable_malloc_free.c tests/runners/test_testable_main_2_runner.c test-production.a
	$(TEST_C_COMPILER) $(TEST_CFLAGS) $(TEST_INC_DIRS) $(TEST_SYMBOLS) $^ -o $@

$(TEST_TARGET3): $(UNITY_ROOT)/src/unity.c tests/test_led.c tests/runners/test_led_runner.c test-production.a
	$(TEST_C_COMPILER) $(TEST_CFLAGS) $(TEST_INC_DIRS) $(TEST_SYMBOLS) $^ -o $@

# ^^^ how to indicate header dependencies?
$(TEST_TARGET4): $(UNITY_ROOT)/src/unity.c tests/test_logger.c tests/runners/test_logger_runner.c tests/logger.c tests/testable_malloc_free.c
	$(TEST_C_COMPILER) $(TEST_CFLAGS) $(TEST_INC_DIRS) $(TEST_SYMBOLS) $^ -o $@

# ^^^ how to indicate header dependencies?
$(TEST_TARGET5): $(UNITY_ROOT)/src/unity.c tests/test_logger_leak.c tests/runners/test_logger_leak_runner.c tests/logger.c tests/fake_testable_malloc_free.c
	$(TEST_C_COMPILER) $(TEST_CFLAGS) $(TEST_INC_DIRS) $(TEST_SYMBOLS) $^ -o $@

# Note that main.test.o and io.test.o are not incorporated into test-production.a.
test-production.a: foo.test.o led.test.o testable_main.test.o
	ar rcs $@ $^

# The .test.o file name ending is chosen
# to avoid conflicting with .o file name ending for target stuff.
%.test.o: %.c
	$(TEST_C_COMPILER) $(TEST_CFLAGS) $(TEST_INC_DIRS) $(TEST_SYMBOLS) -c -o $@ $<

tests/runners/test_testable_main_1_runner.c: tests/test_testable_main.c
	ruby $(UNITY_ROOT)/auto/generate_test_runner.rb tests/test_testable_main.c tests/runners/test_testable_main_1_runner.c

tests/runners/test_testable_main_2_runner.c: tests/test_foo.c
	ruby $(UNITY_ROOT)/auto/generate_test_runner.rb tests/test_foo.c tests/runners/test_testable_main_2_runner.c

tests/runners/test_led_runner.c: tests/test_led.c
	ruby $(UNITY_ROOT)/auto/generate_test_runner.rb tests/test_led.c tests/runners/test_led_runner.c

tests/runners/test_logger_runner.c: tests/test_logger.c
	ruby $(UNITY_ROOT)/auto/generate_test_runner.rb tests/test_logger.c tests/runners/test_logger_runner.c

tests/runners/test_logger_leak_runner.c: tests/test_logger_leak.c
	ruby $(UNITY_ROOT)/auto/generate_test_runner.rb tests/test_logger_leak.c tests/runners/test_logger_leak_runner.c

clean-tests:
	$(CLEANUP) test-production.a *.test.o *$(TEST_TARGET_EXTENSION) tests/runners/*.c

ci: TEST_CFLAGS += -Werror
ci: run-tests

###############################################################################
# target stuff

TARGET_CC=avr-gcc
TARGET_CFLAGS=-Wall -mmcu=atmega328p -DF_CPU=16000000UL

PORT := $(shell ./discover-arduino-uno-r3-port)

burn: main.hex
	@echo The Arduino UNO r3 is on port "'"$(PORT)"'".
	avrdude -v -patmega328p -carduino -b115200 -D -P $(PORT) -Uflash:w:$<:i

clean-target:
	rm -f *.o
	rm -f *.elf
	rm -f *.hex
	rm -f *.s

main.hex: main.elf
main.elf: main.o testable_main.o foo.o io.o led.o

testable_main.o: testable_main.c foo.h testable_main.h
foo.o: foo.c io.h arduino-uno-r3.h atmega328.h led.h testable_main.h
led.o: led.c led.h
io.o: io.c io.h arduino-uno-r3.h atmega328.h

%.s: %.c
	$(TARGET_CC) -c $(TARGET_CFLAGS) $< -S -o $@

%.o: %.c
	$(TARGET_CC) -c $(TARGET_CFLAGS) $< -o $@

%.elf: %.o
	$(TARGET_CC) $(TARGET_CFLAGS) $^ -o $@

%.hex: %.elf
	avr-objcopy -j .text -j .data -O ihex $< $@

###############################################################################
# target and test stuff

clean: clean-target clean-tests
