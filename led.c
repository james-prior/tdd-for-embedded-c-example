#define LED_C
#include <stdint.h>
#include "led.h"

#define LED_ON_MASK ((uint8_t)1 << 5)

void initialize_led(
    volatile uint8_t *led_output_direction,
    volatile uint8_t *led)
{
    *led_output_direction |= LED_ON_MASK;
    turn_off_led(led);
}

void turn_on_led(volatile uint8_t *led)
{
    *led |= LED_ON_MASK;
}

void turn_off_led(volatile uint8_t *led)
{
    *led &= ~LED_ON_MASK;
}
